/**
 *
 * A simple jquery plugin that prevents user data being sent to a
 * third party website without their interaction.
 * Currently it can wrap youtube, vimeo, jamendo, bandcamp and
 * soundcloud
 *
 */

(function($){
  $.fn.mediawrapper = function(options) {
    var video;
    var settings = $.extend({
      'url'  : null,
      'href' : null,
      'icon' : null,
      'embed' : null
    }, options);

    var baseUrl;
    var info_prefix;
    var info_suffix;
    if (typeof Drupal != "undefined") {
      baseUrl = Drupal.settings.mediawrapper.baseUrl;
      info_prefix = Drupal.settings.mediawrapper.info_prefix;
      info_suffix = Drupal.settings.mediawrapper.info_suffix;
    } else {
      var scripts = $('script');
      var baseUrl;
      for (i = 0; i < scripts.length; i++) {
        if (scripts[i].src && (scripts[i].src.indexOf("jQuery-mediaWrapper.js") > -1))
        {
          baseUrl = scripts[i].src.substring(0, scripts[i].src.lastIndexOf("/") + 1);
        }
      }
      info_prefix = 'Bei einem Klick hier werden Daten an';
      info_suffix = '&uuml;bertragen.';
    }

    var url = settings.url;
    var icon = settings.icon;
    var href = settings.url;
    var player = settings.embed;
    if ($(this).get(0).tagName == "SCRIPT") {
      video = jQuery('<div/>', {id: settings.id + '_player'});
      video.insertAfter($(this).get(0));
    } else {
      video = $(this);
    }
    if (video.children("a").size() > 0) {
      href = video.children("a")[0].href;
    if (!url) {
      url = href;
    }
  }
  if (settings.href) {
    href = settings.href;
  }
  var platform = url.substring(url.indexOf(":") + 3);
  if (platform.indexOf("/") > -1) {
    platform = platform.substring(0, platform.indexOf("/"));
  }
  if (platform.indexOf("youtube") > -1) {
      platform = "youtube";
    } else if (platform.indexOf("vimeo") > -1) {
      platform = "vimeo";
    } else if (platform.indexOf("soundcloud") > -1) {
      platform = "soundcloud";
    } else if (platform.indexOf("bandcamp") > -1) {
      platform = "bandcamp";
    } else if (platform.indexOf("jamendo") > -1) {
      platform = "jamendo";
    }

    var videoId = url;
    if (platform == "youtube") {
      videoId = url.substring(url.indexOf("v=") + 2);
      if (videoId.indexOf("&") > -1) {
        videoId = videoId.substring(0, videoId.indexOf("&"));
      }
    } else if (platform == "vimeo") {
      videoId = url.substring(url.lastIndexOf("/") + 1);
    } else if (platform == "jamendo") {
      videoId = url.substring(url.lastIndexOf("/") + 1);
    }

    var loader = $('<a href="' + href + '"></a>');
    var addPlay = false;
    if (!icon) {
      icon = baseUrl + platform + '-icon.png';
    }
    else {
      addPlay = true;
    }
    var span = $('<span class="mediawrapper"/>');
    var img = $('<img src="' + icon + '" alt="Wrapper-Icon" title="' +
      info_prefix + ' ' + platform + ' ' + info_suffix + '"/>');
    span.append(img);
    loader.append(span);
    if (addPlay) {
      var playIcon = $('<img class="playicon" src="' + baseUrl + 'player-blank.gif" alt="Player-Icon"/>');
      span.append(playIcon);
    }
    video.children().remove();
    video.append(loader);
    loader.click(function() {
    if (!player) {
      if (platform == "youtube") {
        player = '<iframe width="420" height="315" src="https://www.youtube-nocookie.com/embed/' + videoId + '?autoplay=1&rel=0" frameborder="0" allowfullscreen></iframe>';
      } else if (platform == "vimeo") {
        player = '<iframe src="http://player.vimeo.com/video/' + videoId + '?title=0&amp;byline=0&amp;portrait=0&amp;autoplay=1" width="400" height="225" frameborder="0" webkitAllowFullScreen mozallowfullscreen allowFullScreen></iframe>';
      } else if (platform == "soundcloud") {
        player = '<iframe width="100%" height="166" scrolling="no" frameborder="no" src="' + videoId + '&auto_play=true"></iframe>';
      } else if (platform == "bandcamp") {
        player = '<iframe width="400" height="100" style="position: relative; display: block; width: 400px; height: 100px;" src="' + videoId + '" allowtransparency="true" frameborder="0"></iframe>'
      } else  if (platform == "jamendo") {
        player = '<object width="200" height="300" classid="clsid:d27cdb6e-ae6d-11cf-96b8-444553540000" codebase="http://fpdownload.macromedia.com/pub/shockwave/cabs/flash/swflash.cab#version=7,0,0,0" align="middle"><param name="allowScriptAccess" value="always" /><param name="wmode" value="transparent" /><param name="movie" value="http://widgets.jamendo.com/de/album/?album_id=' + videoId + '&playerautoplay=1&playertype=2008" /><param name="quality" value="high" /><param name="bgcolor" value="#FFFFFF" /><embed src="http://widgets.jamendo.com/de/album/?album_id=' + videoId + '&playerautoplay=1&playertype=2008" quality="high" wmode="transparent" bgcolor="#FFFFFF" width="200" height="300" align="middle" allowScriptAccess="always" type="application/x-shockwave-flash" pluginspage="http://www.macromedia.com/go/getflashplayer">&nbsp;</embed></object>';
      }
    }
    video.html(player);
    return false;
  });
  return this;
  };
})(jQuery);
