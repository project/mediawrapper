Readme file for the mediaWrapper module for Drupal
--------------------------------------------------

Analog to Social Share Privacy mediaWrapper provides a way to prevent 
visitors' data being sent to third party websites without their 
explicit interaction. Instead of directly loading a media player from 
e.g. youtube.com, visitors see a image with a "play" icon on it. If 
they click this image, the player from the third party is loadad.

To do so a filter is added, that replaces [media]-Tags with some 
jQuery based script and a html fallback for client that have 
Javascript disabled. The filter can be enabled for individual content 
formats.

Nativly supported are http://www.youtube.com/, www.jamendo.com/, 
http://bandcamp.com/, http://vimeo.com and http://soundcloud.com/.

There is also support for oembed in a separate module. If it is 
enabled you can add media from more than 50 diffrent video providers.
Most of them have not been tested, but if one doesn't, just open a
ticket. You can also configure the width of the block that display
the actual content. In native mode there are fix presets.
Find all (possible) supported providers here: http://embed.ly/

Dependencies:
  The native mediaWrapper module has no dependencies, nothing 
  special is required.

  The oembed version relies on imagecache and oembed.


Configuration:
  If the module is installed a filter is available that can be enabled
  in the input formats configuration.
  
  The oembed version allows to configure the maximum width of the 
  embedded content. Native version has fix presets.
